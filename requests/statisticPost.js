const fetch = require('node-fetch')

module.exports = function statisticPost({ url, success, failed }) {
  let newsletter_nid = Math.floor(Math.random() * (100 - 1 + 1)) + 1;
  let device_id = Math.random().toString(36).substr(2);

  return fetch(`${url}/api/v1/url-statistic`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      newsletter_nid,
      device_id,
      "url": "http://babauba.de"
    })
  })
}