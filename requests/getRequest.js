const fetch = require('node-fetch')

module.exports = function getRequest({ url, success, failed }) {
  // let path = `${args.url}?limit=${Math.floor(Math.random() * (100 - 1 + 1)) + 1}&page=${Math.floor(Math.random() * (10 - 1 + 1)) + 1}`

  return fetch(url)
}