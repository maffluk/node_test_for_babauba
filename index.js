const allSettled = require('promise.allsettled')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

let { iterations, baseUrl } = require('./config.json')
let requests = iterations * 4
const tokenPost = require('./requests/tokenPost')
const getRequest = require('./requests/getRequest')
const statisticPost = require('./requests/statisticPost')

let success = 0, failed = 0;

let i = 0

let allPromises = []

const requestDelay = (callback, params, time, message) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      allPromises.push(callback(params)
        .then(res => {
          res.status === 200 ? success++ : failed++;
          console.log(`${message}: ${res.status}`)
        })
        .catch(err => {
          console.log(err.message)
          failed++
        })
        .finally(() => {
          requests--

          if (requests <= 0) {
            allSettled(allPromises)
              .then((promises) => {
                console.log(`Success: ${success} (${success * 100 / promises.length}%); Failed: ${failed} (${failed * 100 / promises.length}%); Requests: ${promises.length}`)
              })
          }
        }))
      resolve()
    }, time)
  })
}

const interval = setInterval(async () => {
  --iterations

  if (iterations >= 0) {
    await requestDelay(statisticPost, { url: baseUrl }, 20, 'Statistic post'),
      await requestDelay(tokenPost, { url: baseUrl }, 20, 'Token post'),
      await requestDelay(getRequest, { url: `${baseUrl}/api/v1/home`, param: false }, 20, 'Get home'),
      await requestDelay(getRequest, { url: `${baseUrl}/api/v1/news-letter`, param: false }, 20, 'Get news-letter')
  }
  else {
    clearInterval(interval)
  }
}, 20);

